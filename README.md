# lufft_ws800_umb

## Requirements
- ROS Noetic, Melodic or Kinetic
- ros-${ROS_DISTRO}-swri-serial-util

## Message
```
channel(dec)	channel(hex)	type		description
100		00 64		float32		current temperature [°C]
101		00 65		float32		current external temperature [°C]
111		00 6F		float32		current wind chill temperature [°C]
200		00 C8		float32		current relative humidity [%]
305		01 13		float32		current relative air press [hPa]
310		06 13		float32		air density [kg/m³]
405		01 95		float32		current wind speed [km/h]
500		01 F4		float32		current wind direction [°]
617		69 02		uint16		lightning events [-] = [0 ~ 255]
620		6C 02		float32		absolute precipitation [mm]
700		02 BC		uint8		current precipitation type [0, 40, 60, 70] 
						: [No precipitation, 
						   Precipitation, 
						   Liquid precipitation e.g. rain, 
						   Solid precipitation e.g. snow]
805		25 03		float32		wind value quality [%]
820		03 34		float32		current precipitation intensity [mm/h]
900		84 03		float32		global radiation [W/m²]
```

## Getting started
`roslaunch lufft_ws800_umb lufft_ws800_umb.launch `
