#include <ros/ros.h>
#include <swri_serial_util/serial_port.h>
#include <lufft_ws800_umb/weather.h>

std::string error_msg;

swri_serial_util::SerialPort serial;

union unionVar
{
  int dec;
  unsigned int hex;
  float fp;
};

enum ReadResult
{
  READ_SUCCESS = 0,
  READ_INSUFFICIENT_DATA = 1,
  READ_TIMEOUT = 2,
  READ_INTERRUPTED = 3,
  READ_ERROR = -1,
  READ_PARSE_FAILED = -2
};

bool CreateSerialConnection(const std::string& device)
{
  swri_serial_util::SerialConfig config;
  config.baud = 19200;
  config.data_bits = 8;
  config.parity = swri_serial_util::SerialConfig::NO_PARITY;
  config.stop_bits = 1;
  config.flow_control = false;
  config.low_latency_mode = false;
  config.writable = true;

  bool success = serial.Open(device, config);

  if (!success)
  {
    error_msg = serial.ErrorMsg();
    std::cout << error_msg << std::endl;
    return false;
  }

  return success;
}

std::vector<uint8_t> HexToBytes(const std::string& hex)
{
  std::vector<uint8_t> bytes;

  for (unsigned int i = 0; i < hex.length(); i += 2)
  {
    std::string byteString = hex.substr(i, 2);
    uint8_t byte = (uint8_t) strtol(byteString.c_str(), NULL, 16);
    bytes.push_back(byte);
  }

  return bytes;
}

bool Write(const std::string& command) {
  std::vector<uint8_t> bytes = HexToBytes(command);

  int32_t written = serial.Write(bytes);
  if (written != (int32_t)command.length() / 2)
  {
    ROS_ERROR("Failed to send command: %s", command.c_str());
  }
  return written == (int32_t)command.length();
}

ReadResult ReadData(std::vector<uint8_t>& data_buffer)
{
  swri_serial_util::SerialPort::Result result =
      serial.ReadBytes(data_buffer, 0, 100);

  if (result == swri_serial_util::SerialPort::ERROR)
  {
    error_msg = serial.ErrorMsg();
    return READ_ERROR;
  }
  else if (result == swri_serial_util::SerialPort::TIMEOUT)
  {
    error_msg = "Timed out waiting for serial device.";
    return READ_TIMEOUT;
  }
  else if (result == swri_serial_util::SerialPort::INTERRUPTED)
  {
    error_msg = "Interrupted during read from serial device.";
    return READ_INTERRUPTED;
  }

  return READ_SUCCESS;
}

int main(int argc, char** argv){
  ros::init(argc, argv, "lufft_ws800_weather_station_node");
  ros::NodeHandle nh("~");

  std::string device;
  nh.getParam("device", device);

  ros::Publisher pub_weather = nh.advertise<lufft_ws800_umb::weather>("/ws800/weather", 1);

  std::cout << "Start lufft WS800 weather station" << std::endl;

  if (CreateSerialConnection(device))
  {
    ROS_INFO("%s connected to device.", device.c_str());

    std::string command = "0110C87001F01F022F100E640065006F00C800310136019501F40125036C02BC023403690284030311BB04";

    while(ros::ok())
    {
      ros::Duration(0.02).sleep();
      ROS_INFO("send message : %s", command.c_str());
      Write(command);

      int len = 0;
      bool header_bytes = false;
      std::vector<uint8_t> rcv_bytes;

      double t0 = ros::Time::now().toNSec();
      while(ros::Time::now().toNSec() - t0 < 5e8)
      {
        std::vector<uint8_t> bytes;
        ReadData(bytes);

        if (!header_bytes && bytes.size() > 7 && (int)bytes[0] == 1 && (int)bytes[1] == 16)
        {
          len = (int)bytes[6];
          rcv_bytes.insert(rcv_bytes.end(), bytes.begin(), bytes.end());
          header_bytes = true;
        }
        else
        {
          if (!bytes.empty())
          {
            rcv_bytes.insert(rcv_bytes.end(), bytes.begin(), bytes.end());

            if ((int)bytes[bytes.size() - 1] == 4)
              break;
          }
        }
      }

      if ((rcv_bytes.size() - 12) == len)
      {
        std::stringstream ss;
        for (auto& byte : rcv_bytes){
          ss << std::hex << std::setw(2) << std::setfill('0') << (int)byte << " ";
        }
        ROS_INFO("received messege : %s", ss.str().c_str());

        lufft_ws800_umb::weather weather_msg;
        weather_msg.header.frame_id = "WS800";
        weather_msg.header.stamp = ros::Time::now();
        weather_msg.header.seq++;

        weather_msg.status = (int)rcv_bytes[10];

        int slice_front = 12;
        std::vector<uint8_t> data_bytes(rcv_bytes.cbegin() + slice_front, rcv_bytes.cend());

        unionVar var;
        bool check_split = false;
        while (rcv_bytes.size() - 4 > slice_front)
        {
          std::vector<uint8_t> data_bytes(rcv_bytes.cbegin() + slice_front, rcv_bytes.cend() - 4);

          if (!check_split)
          {
            var.hex = (data_bytes[0] | data_bytes[1] << 8); // split character, invalid data filter
            if (var.dec == 9219)
              slice_front += 4;
            else
            {
              slice_front += 2;
              check_split = true;
            }
          }
          else
          {
            var.hex = data_bytes[2];
            int type = var.dec;
            float value = -1;
            switch (type)
            {
            case 22:
              var.hex = (data_bytes[3] | data_bytes[4] << 8 | data_bytes[5] << 16 | data_bytes[6] << 24);
              value = var.fp;
              slice_front += 7;
              break;
            case 18:
              var.hex = (data_bytes[3] | data_bytes[4] << 8);
              value = var.dec;
              slice_front += 5;
              break;
            case 16:
              var.hex = data_bytes[3];
              value = var.dec;
              slice_front += 4;
              break;
            }

            var.hex = (data_bytes[0] | data_bytes[1] << 8);
            int channel = var.dec;
            switch (channel)
            {
            case 100:
              weather_msg.temperature = value;
              break;
            case 111:
              weather_msg.wind_temperature = value;
              break;
            case 200:
              weather_msg.humidity = value;
              break;
            case 305:
              weather_msg.air_press = value;
              break;
            case 310:
              weather_msg.air_density = value;
              break;
            case 405:
              weather_msg.wind_speed = value;
              break;
            case 500:
              weather_msg.wind_direction = value;
              break;
            case 617:
              weather_msg.lightning = (uint16_t)value;
              break;
            case 620:
              weather_msg.precipitation = value;
              break;
            case 700:
              weather_msg.precipitation_type = (uint8_t)value;
              break;
            case 805:
              weather_msg.wind_value_quality = value;
              break;
            case 820:
              weather_msg.precipitation_intensity = value;
              break;
            case 900:
              weather_msg.radiation = value;
              break;
            }

            check_split = false;
          }
        }
        pub_weather.publish(weather_msg);
        ros::Duration(0.5).sleep();
      }
    }
  }
  else {
    return -1;
  }

  serial.Close();
  return 0;
}
